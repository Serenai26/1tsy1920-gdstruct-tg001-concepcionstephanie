#include<iostream>

using namespace std;

int recursion(int input)
{
	if (input<1)
	{
		return 0;
	}

	//tasks
	int a;
	int b;
	int c;
	int d;
	int e;

	a = input / 10000;
	input -= (a * 10000);

	b = input / 1000;
	input -= (b * 1000);

	c = input / 100;
	input -= (c * 100);

	d = input / 10;
	input -= (d * 10);

	e = input;

	cout << endl;

	int sum = a + b + c + d + e;
	cout << a << " + " << b << " + " << c << " + " << d << " + " << e << " = " << sum << endl;

	return recursion(input-1);
}

void main()
{
	//compare the sum of digits of a number
	//input: 12345		>>	1+2+3+4+5
	//output: 15
	int input;

	cout << "What numuber do you want to input? " << endl;
	cin >> input;

	recursion(input);

}