#include<iostream>

using namespace std;

void recursion(int posNum)
{
	if (posNum < 1)
	{
		return;
	}

	//tasks
	int i;
	bool primeNum = true;

	//incase I might forget in the future TT^TT
	/*posNum is divided into 2 to make sure if it really is divisible to a
	certain number then compares to i,which is 2, and see if posNum will
	become 0  or 1...1 is prime*/
	for (i = 2; i <= posNum / 2; i++)
	{
		//checking if posNum is divisible to a certain number when Modulo is used
		if (posNum % i == 0)
		{
			primeNum = false;
			break;
		}
	}

	if (primeNum)
	{
		cout << "It is a prime number" << endl;
	}
	else
	{
		cout << "It is not a prime number" << endl;
	}

}

int main()
{
	int posNum;
	
	cout << "Enter a positive number: " << endl;
	cin >> posNum;

	recursion(posNum);

	system("pause");
	return 0;
}