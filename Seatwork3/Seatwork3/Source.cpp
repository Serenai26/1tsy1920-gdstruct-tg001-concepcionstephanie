#include<iostream>
#include<string>
#include<time.h>
#include<sstream>

using namespace std;

void PrintingMembers(string guildName, int guildSize, string *guild)
{
	//printing the members in the guild
	cout << "Members of " << guildName << " : " << endl;

	for (int i = 0; i < guildSize; i++)
	{
		cout << guild[i] << endl;
	}
}

void AddingMember(int guildSize, string guildUsername,string *guild)
{
	for (int i = guildSize; i < guildSize+1; i++)
	{
		i++;
		cout << "Username: ";
		cin >> guildUsername;
		cout << endl;
		guild[i]= guildUsername;

	}
}

void main()
{
	//For no.1
	string guildName;
	//For no.2
	int guildSize;
	//For no.3
	string guildUsername;

	//1. Declare a name for the guild (user input)
	cout << "Greeting user! What is the name of your guild? " << endl;
	cin >> guildName;

	system("pause");
	system("cls");

	cout << "Welcome to " << guildName << "!!! " << endl;

	//2. Declare an initial size for the guild (user input)
	cout << "How many people would you like to be in your guild? " << endl;
	cout << "or what is the initial size of your guild? " << endl;
	cin >> guildSize;

	string* guild = new string[guildSize]; //stores the member's username/s

	//3. Input the usernames of the initial members(user input)
	cout << "What are the usernames of your (initial) members? " << endl;

	//placing the usernames in the Array
	for (int i = 0; i < guildSize; i++)
	{
		cout << "Username: ";
		cin >> guildUsername;
		cout << endl;
		guild[i] = guildUsername;

	}

	system("pause");
	system("cls");


	//printing the members in the guild
	/*cout << "Members of " << guildName << " : " << endl;

	for (int i = 0; i < guildSize; i++)
	{
		cout << guild[i] << endl;
	}*/
	PrintingMembers(guildName,guildSize, guild);

	//operations
	cout << "OPTIONS: " << endl;

	//displaying the options
	int choiceNums[] = { 1,2,3 };
	string choiceString[] = { ": Rename a member" ,": Adding a member" ,": Deleting a member" };
	
	for (int c = 0; c < 3; c++)
	{
		cout << choiceNums[c] << choiceString[c]<<endl;
	}

	//choosing the options
	int choice;
	cout << "What would you choose?" << endl;
	cin >> choice;

	switch (choice)
	{
		case 1:
			break;
		case 2:
			guildSize += 1;
			delete[] guild;
			guild = new string[guildSize];
			for (int i = (guildSize+1); i < guildSize+2; i++)
			{
				cout << "Username: ";
				cin >> guildUsername;
				cout << endl;
				guild[i] = guildUsername;

			}
			
			break;
		case 3:
			guildSize -= 1;
			delete[] guild;
			guild = new string[guildSize];
			
			break;
	default:
		break;
	}

	PrintingMembers(guildName, guildSize, guild);
	delete[] guild;
}