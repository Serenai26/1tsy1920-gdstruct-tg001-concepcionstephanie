#include<iostream>
#include<string>
#include<time.h>
//#include "Header.h"

using namespace std;


int main()
{
	srand(time(NULL));
	int numArray[10];
	int num;
	int i, j;
	int count = 10;
	int temp;
	int choice;

	//randomizing numbers
	for (i = 0; i < count; i++)
	{
		num = rand() % 69 + 1;
		numArray[i] = num;
		//cout << i << " " << numArray[i] << endl;
	}

	cout << "How do you want the numbers to be ordered?" << endl;
	cout << "1 : Ascending" << endl << "2 : Decending" << endl;
	cin >> choice;


	//if ascending
	if (choice == 1)
	{
		cout << "sorting: " << endl;
		//sorting the numbers
		for (i = 0; i < count; i++)
		{
			for (j = 0; j < count - 1; j++)
			{
				if (numArray[j] > numArray[j + 1])
				{
					temp = numArray[j];
					numArray[j] = numArray[j + 1];
					numArray[j + 1] = temp;
				}
			}
		}

		//for printing

		for (i = 0; i < count; i++)
		{
			cout << numArray[i] << endl;
		}
	}
			
			
		//if descending
		else if (choice == 2)
		{
	
			cout << "sorting: " << endl;
			//sorting the numbers
			for (i = 0; i < count; i++)
			{
				for (j = 0; j < count - 1; j++)
				{
					if (numArray[j] < numArray[j + 1])
					{
						temp = numArray[j];
						numArray[j] = numArray[j + 1];
						numArray[j + 1] = temp;
					}
				}
			}

			//for printing

			for (i = 0; i < count; i++)
			{
				cout << numArray[i] << endl;
			}
	}

	system("pause");
	return 0;
}