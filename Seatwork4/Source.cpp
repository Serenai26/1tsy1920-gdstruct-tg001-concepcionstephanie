#include<iostream>
#include<string>
#include<time.h>
#include<assert.h>
#include"Header.h"

using namespace std;

void main()
{
	srand(time(NULL));
	int nums;
	
	int size;
	//asking the size 
	cout << "What is the size or your desire?" << endl;
	cin >> size;
	cout << endl;

	UnorderedArray<int> arrays(size);

	//filling array with random vlaues
	cout << "Unordered Array: " << endl;
	for (int i = 1; i <=size; i++)
	{
		nums = rand() % 100 + 1;
		arrays.push(nums);
	}

	//printing the values
	cout << "  index	number" << endl;
	for (int i = 0; i < arrays.getSize(); i++)
	{
		cout << i <<")	 "<<arrays[i] << endl;
	}

	cout << endl;
	//removing an element in the array
	int choice;
	cout << "Choose an index to remove a number." << endl;
	
	cin >> choice;
	arrays.remove(choice);

	//printing the array after removing an element
	for (int i = 0; i < arrays.getSize(); i++)
	{
		cout << i << " ) " << arrays[i] << endl;
	}

	system("pause");
}