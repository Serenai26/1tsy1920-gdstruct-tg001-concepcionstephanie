#pragma once

#include<assert.h>

template<class T> //allows class to 

class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) :
		mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);

			mGrowSize = ((growBy > 0) ? growBy : 0); // ? is the in-line if-else statement		if true, goes to growBy. if false, goes to 0

		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL); //if condition true, continue. if condition false, break or stops working.


		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
		{
			mNumElements--;
		}
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)// index is more than the current size
		{
			return;
		}
		for (int i = index; i < mMaxSize; i++)
		{
			mArray[i] = mArray[i + 1];
		}
		mMaxSize--;

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize;
		}
	}

	virtual void search()
	{
		for (int i = 0; i < mMaxSize; i++)
		{
			for (int j = i + 1; j < mMaxSize; j++)
			{
				int temp = 0;
				if (i > j)
				{
					temp = i;
					i = j;
					j = temp;
				}

			}
		}

		if (value == true)
		{
			return index;
		}
		else
		{
			cout << "Value not found." << endl;
		}
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize); //memcpy is member copy --simpler verion of for loop

		delete[]mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
	}
};